<?php
/*
 * Copyright (c) 2021.
 */

namespace App\Interfaces;

/**
 * interface ProductInterface.
 */
interface ProductInterface
{

    /**
     * Set name of product.
     *
     * @param string $name
     */
    public function setName(string $name);

}