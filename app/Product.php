<?php

/*
 * Copyright (c) 2021.
 */
namespace App;


use App\Interfaces\ProductInterface;

class Product implements  ProductInterface
{
    /**
     * Name of product in shop.
     *
     * @var string $name
     */

    protected string $name;
    /**
     * Price of products.
     *
     * @var float $price
     */
    protected float $price;
    /**
     * Prise with discount by pac.
     *
     * @var  float $pacPrice
     */
    protected float $pacPrice;
    /**
     * Count products in one pac.
     *
     * @var int $inPack
     */
    protected int $inPack;


    /**
     * Set name of product.
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Get name.
     *
     * @return string $name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Price of products
     *
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Set price of packet each product.
     *
     * @param float $pacPrice
     */
    public function setPacPrice(float $pacPrice)
    {
        $this->pacPrice = $pacPrice;
    }

    /**
     * Get price of packet each product.
     *
     * @return float $pacPrice
     */
    public function getPacPrice(): float
    {
        return $this->pacPrice;
    }

    /**
     *Set number of products in package.
     *
     * @param integer $inPack
     */
    public function  setInPack(int $inPack)
    {
        $this->inPack = $inPack;
    }

    /**
     *Get number of products in package.
     *
     * @return integer
     */
    public function getInPack(): int
    {
        return $this->inPack;
    }

}


