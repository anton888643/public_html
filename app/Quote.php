<?php
/*
 * Copyright (c) 2021.
 */
//require_once ('Pricing.php');

namespace App;

use App\Interfaces\ProductInterface;

/**
 * class Quote
 */

class Quote
{
    /**
     * List products.
     *
     * @var array
     */
    protected array $listOfProducts = [];

    /**
     * Add product to quote.
     *
     * @param ProductInterface $product
     */
    public function addToQuote(ProductInterface $product)
    {
       $this->listOfProducts[] = $product;
    }

    /**
     * Get list of all products.
     *
     * @return array
     */
    public function getList(): array
    {
        return $this->listOfProducts;
    }

    /**
     * @throws \Exception
     */
    public function getById($id)
    {
        try {
            if (!isset($this->listOfProducts[$id])) {
                throw new \Exception("Error");
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $this->listOfProducts[$id];
    }

    public function  inMag()

    {
        $prodMag =
            [
                array_values((array)($this->getById("0")))[0]=>[array_values((array)($this->getById("0")))["1"], array_values((array)($this->getById("0")))["2"], array_values((array)($this->getById("0")))["3"]],
                array_values((array)($this->getById("1")))[0]=>[array_values((array)($this->getById("1")))["1"], array_values((array)($this->getById("1")))["2"], array_values((array)($this->getById("1")))["3"]],
                array_values((array)($this->getById("2")))[0]=>[array_values((array)($this->getById("2")))["1"], array_values((array)($this->getById("2")))["2"], array_values((array)($this->getById("2")))["3"]],
                array_values((array)($this->getById("3")))[0]=>[array_values((array)($this->getById("3")))["1"], array_values((array)($this->getById("3")))["2"], array_values((array)($this->getById("3")))["3"]]
            ];
        return $prodMag;
    }

        public function getTotalPrice($product): string
    {

        $price = [];
        foreach ($this->inMag() as $key => $value){
            $calcProd = substr_count($product->getName(),$key);
            if ($calcProd > 0){
                $price[] = intdiv($calcProd,$value[2]) * $value[1] + $calcProd % $value[2] * $value[0];
            }else{
                $price[] = 0;
            }
        }
        return  array_sum($price) ;
    }

}

